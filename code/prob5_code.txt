[
    20,
    20,
    19,
    0,
    -1,
    1,
    {
        "opcode": "LD_REL",
        "args": [
            0
        ]
    },
    {
        "opcode": "MOD",
        "args": [
            2
        ]
    },
    {
        "opcode": "CMP",
        "args": [
            3
        ]
    },
    {
        "opcode": "JE",
        "args": [
            14
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            0
        ]
    },
    {
        "opcode": "ADD",
        "args": [
            1
        ]
    },
    {
        "opcode": "WR",
        "args": [
            0
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            6
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            0
        ]
    },
    {
        "opcode": "WR",
        "args": [
            1
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            2
        ]
    },
    {
        "opcode": "ADD",
        "args": [
            4
        ]
    },
    {
        "opcode": "WR",
        "args": [
            2
        ]
    },
    {
        "opcode": "CMP",
        "args": [
            5
        ]
    },
    {
        "opcode": "JE",
        "args": [
            22
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            6
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            0
        ]
    },
    {
        "opcode": "OUT",
        "args": [
            -1
        ]
    },
    {
        "opcode": "HLT",
        "args": []
    }
]