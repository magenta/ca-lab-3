[
    19,
    "W",
    "h",
    "a",
    "t",
    " ",
    "i",
    "s",
    " ",
    "y",
    "o",
    "u",
    "r",
    " ",
    "n",
    "a",
    "m",
    "e",
    "?",
    " ",
    7,
    "H",
    "e",
    "l",
    "l",
    "o",
    ",",
    " ",
    1,
    "!",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "\n",
    {
        "opcode": "LD_REL",
        "args": [
            0
        ]
    },
    {
        "opcode": "WR",
        "args": [
            30
        ]
    },
    {
        "opcode": "LD_ABS",
        "args": [
            0
        ]
    },
    {
        "opcode": "INC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            33
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            30
        ]
    },
    {
        "opcode": "CMP",
        "args": [
            36
        ]
    },
    {
        "opcode": "JE",
        "args": [
            56
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            33
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            "AC"
        ]
    },
    {
        "opcode": "OUT",
        "args": [
            -1
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            33
        ]
    },
    {
        "opcode": "INC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            33
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            30
        ]
    },
    {
        "opcode": "DEC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            30
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            43
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            20
        ]
    },
    {
        "opcode": "WR",
        "args": [
            31
        ]
    },
    {
        "opcode": "LD_ABS",
        "args": [
            20
        ]
    },
    {
        "opcode": "INC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            34
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            62
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            31
        ]
    },
    {
        "opcode": "CMP",
        "args": [
            36
        ]
    },
    {
        "opcode": "JE",
        "args": [
            75
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            34
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            "AC"
        ]
    },
    {
        "opcode": "OUT",
        "args": [
            -1
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            34
        ]
    },
    {
        "opcode": "INC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            34
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            31
        ]
    },
    {
        "opcode": "DEC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            31
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            62
        ]
    },
    {
        "opcode": "IN",
        "args": [
            -1
        ]
    },
    {
        "opcode": "CMP",
        "args": [
            37
        ]
    },
    {
        "opcode": "JE",
        "args": [
            80
        ]
    },
    {
        "opcode": "OUT",
        "args": [
            -1
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            75
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            28
        ]
    },
    {
        "opcode": "WR",
        "args": [
            32
        ]
    },
    {
        "opcode": "LD_ABS",
        "args": [
            28
        ]
    },
    {
        "opcode": "INC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            35
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            86
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            32
        ]
    },
    {
        "opcode": "CMP",
        "args": [
            36
        ]
    },
    {
        "opcode": "JE",
        "args": [
            99
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            35
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            "AC"
        ]
    },
    {
        "opcode": "OUT",
        "args": [
            -1
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            35
        ]
    },
    {
        "opcode": "INC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            35
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            32
        ]
    },
    {
        "opcode": "DEC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            32
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            86
        ]
    },
    {
        "opcode": "HLT",
        "args": []
    }
]