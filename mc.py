from enum import Enum
from isa import Opcode


class MC(Enum):
    INSTR_FETCH = "IN_FETCH"

    ALU_RIGHT_MUX_ZERO = "ALU_RIGHT_MUX_ZERO"
    ALU_RIGHT_MUX_MEM = "ALU_RIGHT_MUX_MEM"

    ALU_LEFT_MUX_ZERO = "ALU_LEFT_MUX_ZERO"
    ALU_LEFT_MUX_ACC = "ALU_LEFT_MUX_ACC"

    ALU_SUB = "ALU_SUB"
    ALU_ADD = "ALU_ADD"
    ALU_INC = "ALU_INC"
    ALU_DEC = "ALU_DEC"
    ALU_MOD = "ALU_MOD"

    ACC_MUX_ALU = "ACC_MUX_ALU"
    ACC_MUX_MEM = "ACC_MUX_MEM"
    ACC_MUX_INSTR_ADDR_PART = "ACC_MUX_INSTR_ADDR_PART"
    ACC_MUX_INPUT = "ACC_MUX_INPUT"

    ACC_WRITE_INTO_MEM = "ACC_WRITE_INTO_MEM"
    ACC_OUTPUT = "ACC_OUTPUT"

    IP_MUX_INC = "IP_MUX_INC"
    IP_MUX_INSTR_ADDR_PART = "IP_MUX_INSTR_ADDR_PART"

    ADDR_MUX_INSTR_ADDR_PART = "ADDR_MUX_INSTR_ADDR_PART"
    ADDR_MUX_ACC = "ADDR_MUX_ACC"

    ACC_LATCH = "ACC_LATCH"
    IP_LATCH = "IP_LATCH"
    ADDR_LATCH = "ADDR_LATCH"

    Z_SET_GOTO = "Z_SET_GOTO"
    N_SET_GOTO = "N_SET_GOTO"
    GOTO = "GOTO"
    CMP_INSTR_NOT_EQ_GOTO = "CMP_INSTR_NOT_EQ_GOTO"
    CMP_INSTR_ARG_NOT_EQ_GOTO = "CMP_INSTR_ARG_NOT_EQ_GOTO"

    STOP = "STOP"
    DECODING_ERR = "DECODING_ERR"


class MCAddresses(Enum):
    NEXT_INSTR_FETCH_COMMAND = 0
    IP_INC_COMMAND = 86
    PART_IP_COMMAND = 89
    ERROR_COMMAND = 92
    HLT_COMMAND = 93


mc_memory = [
    {"opcode": MC.INSTR_FETCH, "args": [], "tick_num": 0},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.CMP, 8], "tick_num": 1},
    {"opcode": MC.ADDR_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 1},
    {"opcode": MC.ADDR_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.ALU_RIGHT_MUX_MEM, "args": [], "tick_num": 2},
    {"opcode": MC.ALU_LEFT_MUX_ACC, "args": [], "tick_num": 2},
    {"opcode": MC.ALU_SUB, "args": [], "tick_num": 2},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 2},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.JE, 11], "tick_num": 1},
    {"opcode": MC.Z_SET_GOTO, "args": [MCAddresses.PART_IP_COMMAND.value], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 1},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.LD_ABS, 15], "tick_num": 1},
    {"opcode": MC.ACC_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 1},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.LD_REL, 24], "tick_num": 1},
    {"opcode": MC.CMP_INSTR_ARG_NOT_EQ_GOTO, "args": ["AC", 19], "tick_num": 1},
    {"opcode": MC.ADDR_MUX_ACC, "args": [], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [20], "tick_num": 1},
    {"opcode": MC.ADDR_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 1},
    {"opcode": MC.ADDR_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_MUX_MEM, "args": [], "tick_num": 2},
    {"opcode": MC.ACC_LATCH, "args": [], "tick_num": 2},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 2},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.WR, 29], "tick_num": 1},
    {"opcode": MC.ADDR_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 1},
    {"opcode": MC.ADDR_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_WRITE_INTO_MEM, "args": [], "tick_num": 2},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 2},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.INC, 36], "tick_num": 1},
    {"opcode": MC.ALU_LEFT_MUX_ACC, "args": [], "tick_num": 1},
    {"opcode": MC.ALU_RIGHT_MUX_ZERO, "args": [], "tick_num": 1},
    {"opcode": MC.ALU_INC, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_MUX_ALU, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 1},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.DEC, 43], "tick_num": 1},
    {"opcode": MC.ALU_LEFT_MUX_ACC, "args": [], "tick_num": 1},
    {"opcode": MC.ALU_RIGHT_MUX_ZERO, "args": [], "tick_num": 1},
    {"opcode": MC.ALU_DEC, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_MUX_ALU, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 1},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.MOD, 52], "tick_num": 1},
    {"opcode": MC.ADDR_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 1},
    {"opcode": MC.ADDR_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.ALU_LEFT_MUX_ACC, "args": [], "tick_num": 2},
    {"opcode": MC.ALU_RIGHT_MUX_MEM, "args": [], "tick_num": 2},
    {"opcode": MC.ALU_MOD, "args": [], "tick_num": 2},
    {"opcode": MC.ACC_MUX_ALU, "args": [], "tick_num": 2},
    {"opcode": MC.ACC_LATCH, "args": [], "tick_num": 2},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 2},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.JMP, 54], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [MCAddresses.PART_IP_COMMAND.value], "tick_num": 1},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.ADD, 63], "tick_num": 1},
    {"opcode": MC.ADDR_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 1},
    {"opcode": MC.ADDR_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.ALU_LEFT_MUX_ACC, "args": [], "tick_num": 2},
    {"opcode": MC.ALU_RIGHT_MUX_MEM, "args": [], "tick_num": 2},
    {"opcode": MC.ALU_ADD, "args": [], "tick_num": 2},
    {"opcode": MC.ACC_MUX_ALU, "args": [], "tick_num": 2},
    {"opcode": MC.ACC_LATCH, "args": [], "tick_num": 2},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 2},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.SUB, 72], "tick_num": 1},
    {"opcode": MC.ADDR_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 1},
    {"opcode": MC.ADDR_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.ALU_LEFT_MUX_ACC, "args": [], "tick_num": 2},
    {"opcode": MC.ALU_RIGHT_MUX_MEM, "args": [], "tick_num": 2},
    {"opcode": MC.ALU_SUB, "args": [], "tick_num": 2},
    {"opcode": MC.ACC_MUX_ALU, "args": [], "tick_num": 2},
    {"opcode": MC.ACC_LATCH, "args": [], "tick_num": 2},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 2},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.OUT, 77], "tick_num": 1},
    {"opcode": MC.ADDR_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 1},
    {"opcode": MC.ADDR_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_OUTPUT, "args": [], "tick_num": 2},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 2},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.IN, 81], "tick_num": 1},
    {"opcode": MC.ACC_MUX_INPUT, "args": [], "tick_num": 1},
    {"opcode": MC.ACC_LATCH, "args": [], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 1},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.JG, 84], "tick_num": 1},
    {"opcode": MC.N_SET_GOTO, "args": [MCAddresses.PART_IP_COMMAND.value], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [MCAddresses.IP_INC_COMMAND.value], "tick_num": 1},
    {"opcode": MC.CMP_INSTR_NOT_EQ_GOTO, "args": [Opcode.HLT, MCAddresses.ERROR_COMMAND.value], "tick_num": 1},
    {"opcode": MC.GOTO, "args": [MCAddresses.HLT_COMMAND.value], "tick_num": 1},
    {"opcode": MC.IP_MUX_INC, "args": [], "tick_num": 3},
    {"opcode": MC.IP_LATCH, "args": [], "tick_num": 3},
    {"opcode": MC.GOTO, "args": [MCAddresses.NEXT_INSTR_FETCH_COMMAND.value], "tick_num": 3},
    {"opcode": MC.IP_MUX_INSTR_ADDR_PART, "args": [], "tick_num": 3},
    {"opcode": MC.IP_LATCH, "args": [], "tick_num": 3},
    {"opcode": MC.GOTO, "args": [MCAddresses.NEXT_INSTR_FETCH_COMMAND.value], "tick_num": 3},
    {"opcode": MC.DECODING_ERR, "args": [], "tick_num": 3},
    {"opcode": MC.STOP, "args": [], "tick_num": 3},
]
