import unittest
import logging
import machine


class MachineTest(unittest.TestCase):
    def test_hello(self):
        output = machine.main(["code/hello_pascal_code.txt", "io/hello_input.txt"])
        self.assertEqual(output, "Hello world!")

    def test_cat(self):
        output = machine.main(["code/cat_code.txt", "io/cat_input.txt"])
        with open("io/cat_input.txt", "rt", encoding="utf8") as f:
            sample_output = f.read()
            self.assertEqual(output, sample_output)

    def test_prob5(self):
        output = machine.main(["code/prob5_code.txt", "io/prob5_input.txt"])
        sample_output = str(232792560)
        self.assertEqual(output, sample_output)

    def test_user_name(self):
        output = machine.main(["code/hello_user_code.txt", "io/hello_user_input.txt"])
        self.assertEqual(output, "What is your name? Hello, Alina!")


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    unittest.main()
