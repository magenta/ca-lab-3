.data:
result NUM 20
sum NUM 20
curr NUM 19
zero NUM 0
one NUM -1
o NUM 1
.text:
loop:
ld [result]
mod curr
cmp zero
je next
ld [result]
add sum
wr result
jmp loop
next:
ld [result]
wr sum
ld [curr]
add one
wr curr
cmp o
je finish
jmp loop
finish:
ld [result]
out
hlt
