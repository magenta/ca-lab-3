# Архитектура компьютера: лабораторная работа №3

- Смирнова Алина Юрьевна, P33301
- ```asm | acc | neum | mc | tick | struct | stream | port | ptsr | prob5```
- Без усложнения

## Язык программирования

### Синтаксис

**Форма Бэкуса-Наура:**

```ebnf
<program> ::= <section_list> <EOF>
<section_list> ::= <section> <section_list> | <section>
<section> ::= "." <identifier> <EOL> <line_list>
<line_list> ::= <line> <line_list> | <line>
<line> ::= <line_instruction> <EOL> | <EOL>
<line_instruction> ::= <label_decl> | <instruction> | <variable_declaration>
<variable_declaration> ::= <identifier> <type> <letter_or_number_list>
<label_decl> ::= <label> ":"
<label> ::= <identifier>
<identifier> ::= <letter> | <letter> <letter_or_number_list>

<instruction> ::= <no_arg_instr> | <one_arg_instr>
<no_arg_instr> ::= <no_arg_op>
<one_arg_instr> ::= <one_arg_op> <operand>
<operand> ::= <relative_operand> | <abs_operand>
<abs_operand> ::= <label> | <immediate>
<relative_operand> ::= "[" <label> "]"
<immediate> ::= <int_lit> | <char_lit>

<int_lit> ::= <number_list> | "-" <number_list>
<number_list> ::= <number> <number_list> | <number>
<char_lit> ::= "'" <letter> "'" | "'" <number> "'"

<letter_or_number_list> ::= <letter_or_number> | <letter_or_number> <letter_or_number_list>
<letter_or_number> ::= <letter> | <number>

<letter> ::= [a-z] | [A-Z]
<number> ::= [0-9]

<EOF> ::= "1A"
<EOL> ::= "\n"

<type> ::= "STRING" | "NUM" | "CHAR"
<no_arg_op> ::= "inc" | "dec" | "in" | "out" | "hlt"
<one_arg_op> ::= "add" | "sub" | "mod" | "ld" | "cmp" | "je" | "jg" | "jmp" | "wr"
```


- Код ассемблера включает 2 секции: .data (данные) и .text (код).

Ни команды, ни объявления переменных не могут располагаться вне вышеупомянутых секций.
Секция данных должна располагаться выше секции кода.

- Поддерживаются метки. Единственное слово в строке, оканчивающееся на ":", считается меткой.
- Строки ".data:" и ".text:" считаются особыми метками (они объявляют секции). На них нельзя ссылаться из кода программы.
- Секция кода не должна быть пустой.

- ac/AC - особое слово, обозначающее регистр acc 
```
ld [ac]
```

- Данные бывают трех типов: STRING (паскалевские строки), CHAR (символы), NUM (целые числа). Тип должен быть указан после названия переменной. Символы и строки должны быть заключены в двойные кавычки. При записи в память строки разбиваются посимвольно.

- Точка входа в программу - первая инструкция после метки .text

Пример программы prob5:
```
.data:
result NUM 20
sum NUM 20
curr NUM 19
zero NUM 0
one NUM -1
o NUM 1
.text:
loop:
ld [result]
mod curr
cmp zero
je next
ld [result]
add sum
wr result
jmp loop
next:
ld [result]
wr sum
ld [curr]
add one
wr curr
cmp o
je finish
jmp loop
finish:
ld [result]
out
hlt

```

Память выделяется статически, при запуске модели.


## Организация памяти

- Размер машинного слова - 32 бит.
- Согласно архитектуре фон Неймана, команды и данные лежат в одной памяти.
- Поддерживается косвенная адресация относительно адреса, лежащего в аккумуляторе:
```
ld [ac]
```
- Аналогичный синтаксис в случае с переменными говорит загрузить в аккумулятор значение переменной, а не ее адрес:
```
ld [var]
```

### Система команд

Регистры:
- acc - аккумулятор
- ip - instruction pointer. Используется в цикле выборки инструкции
- addr - регистр адреса. Используется в работе с памятью
- z, n - перезаписвается по результатам операции АЛУ
- mc_pointer - указатель на следующую микрокоманду в памяти микрокоманд (внутренний регистр ControlUnit)

Размер памяти задается при ее инициализации (по дефолту 2048).


### Набор инструкций

Все инструкции, требующие участие двух аргументов, берут значение из аккумулятора и из заданной в качестве аргумента ячейки памяти, а после выполняют с ними действия.
Операции, выполняемые по тактам:
- Выборка инструкции (0 такт)
- Выборка адреса (если требуется) и исполнение команды (1 / 1 и 2 такт)
- Изменение ip (2 / 3 такт)
Таким образом, исполнение 1 команды в модели занимает 3-4 такта

| Синтаксис       | Такты                | Комментарий |
| :------------- |:------------------|:------------------|
| IN     | 3 | Чтение с устроства ввода|
| OUT     | 4 | Запись в устройство вывода|
| CMP <label>   | 4    | Выполняется как вычитание без записи в acc|
| MOD <label>     | 4 | Остаток от деления записывается в acc|
| SUB <label>     | 4 | Значение acc  -= операнд из памяти|
| ADD <label>     | 4 | Значение acc  += операнд из памяти|
| JE <label>     | 3 | Если z=1, ip меняется на адресную часть JE|
| JG <label>     | 3 | Если n=1, ip меняется на адресную часть JG|
| LD <[ac]/[label]/label>     | 3/4 | Адрес операнда или сам операнд записывается в acc|
| WR <label>     | 4 | Значение из acc записывается в память|
| INC     | 3 | Значение acc ++|
| DEC     | 3 | Значение acc --|
| JMP <label>     | 3 | ip меняется на адресную часть JMP|
| HLT    | 3 | Останов|

### Кодирование инструкций

- Машинный код сериализуется в список JSON.

- В этом же списке находятся данные (они лежат перед командами)

- Инструкция представляется словарем, элемент данных - символом или интом

- LD заменяется на LD_ABS или LD_REL. LD_REL работает с памятью и исполняется дольше на 1 такт.

### Память микрокоманд
- Память микрокоманд представляет собой циклическую программу

- В ней находится микропрограмма, которая исполняется для каждой инструкции.

- Цикл выборки и исполнения инструкций продолжается до тех пор, пока не произойдет ошибка или пока не будет вызвана команда останова.

- Микрокоманды могут быть управляющими (частями модели - мультиплексорами) и служебными (команды условного перехода для продвижения внутри памяти микрокоманд). 

| Микрокоманды       | Вид                | Комментарий |
| :------------- |:------------------|:------------------|
| ALU_RIGHT_MUX_ZERO/ALU_RIGHT_MUX_MEM     | Сигнал к мультиплексору правого входа АЛУ |
| ALU_LEFT_MUX_ZERO/ALU_LEFT_MUX_ACC       | Сигнал к мультиплексору левого входа АЛУ | 
| ALU_SUB/ALU_ADD/ALU_INC/ALU_DEC/ALU_MOD       | Сигнал выбора операции АЛУ | 
| ACC_MUX_ALU/ACC_MUX_MEM/ACC_MUX_INSTR_ADDR_PART/ACC_MUX_INPUT       | Сигнал к мультиплексору АСС |
| ACC_WRITE_INTO_MEM       | Сигнал записи АСС в память | 
| IP_MUX_INC/IP_MUX_INSTR_ADDR_PART       | Сигнал к мультиплексору IP | 
| ADDR_MUX_INSTR_ADDR_PART/ADDR_MUX_ACC       | Сигнал к мультиплексору ADDR | 
| ACC_LATCH/IP_LATCH/ADDR_LATCH       | Сигнал фиксации значений в регистрах | 
| Z_SET_GOTO/GOTO/CMP_INSTR_NOT_EQ_GOTO/CMP_INSTR_ARG_NOT_EQ_GOTO      | Команды перехода в памяти микрокоманд |
| STOP/DECODING_ERR      | Команды останова и ошибки декодирования инструкций | 

## Транслятор

- Парсинг кода построчно
- Выделение меток из кода, проверка их корректности (не совпадает с названиями команд, отсутствуют дубликаты)
- При наличии команд, ссылающихся на еще не определенную метку, эта метка добавляется в словарь зависимостей. Она будет удалена оттуда, как только встретится в коде. Пустой словарь зависимостей - успешное завершение трансляции.

Каждая строка в исходном файле может быть:
- пустой (игнорируется)
- меткой
- объявлением секции (особой меткой)
- командой
- объявлением переменной

## Модель процессора

### Схема

[Изображение](https://gitlab.se.ifmo.ru/magenta/ca-lab-3/-/blob/master/schema.jpg) общей схемы модели.


Особенности работы модели:

- Цикл симуляции осуществляется в функции 'simulate'
- Шаг моделирования соответствует одному такту процессора с выводом состояния в журнал
- Для журнала состояний процессора используется стандартный модуль `logging`
- Количество инструкций для моделирования лимитировано
- Остановка моделирования осуществляется при помощи исключения:
    + StopIteration -- если выполнена инструкция halt.

## Тестирование

Реализованные программы 

1. [hello world](https://gitlab.se.ifmo.ru/magenta/ca-lab-3/-/blob/master/asm_src/hello_pascal.txt): вывести на экран строку `'Hello World!'`
2. [cat](https://gitlab.se.ifmo.ru/magenta/ca-lab-3/-/blob/master/asm_src/cat.txt): программа `cat`, повторяем ввод на выводе.
3. [hello_username](https://gitlab.se.ifmo.ru/magenta/ca-lab-3/-/blob/master/asm_src/hello_user.txt) -- программа `hello_username`: запросить у пользователя его имя, считать его, вывести на экран приветствие
4. [prob5](https://gitlab.se.ifmo.ru/magenta/ca-lab-3/-/blob/master/asm_src/prob5.txt): найти наименьшее положительное число, которое делится без остатка на все числа от 1 до 20.


CI:

``` yaml
lab3-example:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - cd src/brainfuck
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --check .
```

где:

- `ryukzak/python-tools` -- docker образ, который содержит все необходимые для проверки утилиты.
  Подробнее: [Dockerfile](/src/brailfuck/Dockerfile)
- `poetry` -- управления зависимостями для языка программирования Python.
- `coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `ruff` -- утилита для форматирования и проверки стиля кодирования


Пример работы транслятора:
```
> ./translator.py asm_src/hello_pascal.txt code/hello_pascal_code.txt

> cat asm_src/hello_pascal.txt

.data:
hw string 12, "Hello world!"
len num 0
addr num 0
count num 0
.text:
ld [hw]
wr len
loop:
ld [len]
cmp count
je end
ld [addr]
inc
ld [ac]
out
ld [addr]
inc
wr addr
ld [len]
dec
wr len
jmp loop
end:
hlt


> cat code/hello_pascal_code.txt

[
    12,
    "H",
    "e",
    "l",
    "l",
    "o",
    " ",
    "w",
    "o",
    "r",
    "l",
    "d",
    "!",
    0,
    0,
    0,
    {
        "opcode": "LD_REL",
        "args": [
            0
        ]
    },
    {
        "opcode": "WR",
        "args": [
            13
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            13
        ]
    },
    {
        "opcode": "CMP",
        "args": [
            15
        ]
    },
    {
        "opcode": "JE",
        "args": [
            32
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            14
        ]
    },
    {
        "opcode": "INC",
        "args": []
    },
    {
        "opcode": "LD_REL",
        "args": [
            "AC"
        ]
    },
    {
        "opcode": "OUT",
        "args": [
            -1
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            14
        ]
    },
    {
        "opcode": "INC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            14
        ]
    },
    {
        "opcode": "LD_REL",
        "args": [
            13
        ]
    },
    {
        "opcode": "DEC",
        "args": []
    },
    {
        "opcode": "WR",
        "args": [
            13
        ]
    },
    {
        "opcode": "JMP",
        "args": [
            18
        ]
    },
    {
        "opcode": "HLT",
        "args": []
    }
]

```

Пример работы программы Hello world:

```
DEBUG - {TICK: 1, ADDR: 0, IP: 16, ACC: 0, Z: 0, N: 0} LD_REL 0
DEBUG - {TICK: 2, ADDR: 0, IP: 16, ACC: 0, Z: 0, N: 0} LD_REL 0
DEBUG - {TICK: 3, ADDR: 0, IP: 16, ACC: 12, Z: 0, N: 0} LD_REL 0
DEBUG - {TICK: 4, ADDR: 0, IP: 17, ACC: 12, Z: 0, N: 0} LD_REL 0
DEBUG - 
DEBUG - {TICK: 5, ADDR: 0, IP: 17, ACC: 12, Z: 0, N: 0} WR 13
DEBUG - {TICK: 6, ADDR: 13, IP: 17, ACC: 12, Z: 0, N: 0} WR 13
DEBUG - {TICK: 7, ADDR: 13, IP: 17, ACC: 12, Z: 0, N: 0} WR 13
DEBUG - {TICK: 8, ADDR: 13, IP: 18, ACC: 12, Z: 0, N: 0} WR 13
DEBUG - 
DEBUG - {TICK: 9, ADDR: 13, IP: 18, ACC: 12, Z: 0, N: 0} LD_REL 13
DEBUG - {TICK: 10, ADDR: 13, IP: 18, ACC: 12, Z: 0, N: 0} LD_REL 13
DEBUG - {TICK: 11, ADDR: 13, IP: 18, ACC: 12, Z: 0, N: 0} LD_REL 13
DEBUG - {TICK: 12, ADDR: 13, IP: 19, ACC: 12, Z: 0, N: 0} LD_REL 13
DEBUG - 
DEBUG - {TICK: 13, ADDR: 13, IP: 19, ACC: 12, Z: 0, N: 0} CMP 15
DEBUG - {TICK: 14, ADDR: 15, IP: 19, ACC: 12, Z: 0, N: 0} CMP 15
DEBUG - {TICK: 15, ADDR: 15, IP: 19, ACC: 12, Z: 0, N: 0} CMP 15
DEBUG - {TICK: 16, ADDR: 15, IP: 20, ACC: 12, Z: 0, N: 0} CMP 15
DEBUG - 

...

DEBUG - {TICK: 592, ADDR: 12, IP: 24, ACC: !, Z: 0, N: 0} OUT -1
DEBUG - OUTPUT: !
DEBUG - {TICK: 593, ADDR: -1, IP: 24, ACC: !, Z: 0, N: 0} OUT -1
DEBUG - {TICK: 594, ADDR: -1, IP: 24, ACC: !, Z: 0, N: 0} OUT -1
DEBUG - {TICK: 595, ADDR: -1, IP: 25, ACC: !, Z: 0, N: 0} OUT -1
DEBUG - 
DEBUG - {TICK: 596, ADDR: -1, IP: 25, ACC: !, Z: 0, N: 0} LD_REL 14
DEBUG - {TICK: 597, ADDR: 14, IP: 25, ACC: !, Z: 0, N: 0} LD_REL 14
DEBUG - {TICK: 598, ADDR: 14, IP: 25, ACC: 11, Z: 0, N: 0} LD_REL 14
DEBUG - {TICK: 599, ADDR: 14, IP: 26, ACC: 11, Z: 0, N: 0} LD_REL 14
DEBUG - 
DEBUG - {TICK: 600, ADDR: 14, IP: 26, ACC: 11, Z: 0, N: 0} INC no arg
DEBUG - {TICK: 601, ADDR: 14, IP: 26, ACC: 12, Z: 0, N: 0} INC no arg
DEBUG - {TICK: 602, ADDR: 14, IP: 27, ACC: 12, Z: 0, N: 0} INC no arg
DEBUG - 
DEBUG - {TICK: 603, ADDR: 14, IP: 27, ACC: 12, Z: 0, N: 0} WR 14
DEBUG - {TICK: 604, ADDR: 14, IP: 27, ACC: 12, Z: 0, N: 0} WR 14
DEBUG - {TICK: 605, ADDR: 14, IP: 27, ACC: 12, Z: 0, N: 0} WR 14
DEBUG - {TICK: 606, ADDR: 14, IP: 28, ACC: 12, Z: 0, N: 0} WR 14
DEBUG - 
DEBUG - {TICK: 607, ADDR: 14, IP: 28, ACC: 12, Z: 0, N: 0} LD_REL 13
DEBUG - {TICK: 608, ADDR: 13, IP: 28, ACC: 12, Z: 0, N: 0} LD_REL 13
DEBUG - {TICK: 609, ADDR: 13, IP: 28, ACC: 1, Z: 0, N: 0} LD_REL 13
DEBUG - {TICK: 610, ADDR: 13, IP: 29, ACC: 1, Z: 0, N: 0} LD_REL 13
DEBUG - 
DEBUG - {TICK: 611, ADDR: 13, IP: 29, ACC: 1, Z: 0, N: 0} DEC no arg
DEBUG - {TICK: 612, ADDR: 13, IP: 29, ACC: 0, Z: 1, N: 0} DEC no arg
DEBUG - {TICK: 613, ADDR: 13, IP: 30, ACC: 0, Z: 1, N: 0} DEC no arg
DEBUG - 
DEBUG - {TICK: 614, ADDR: 13, IP: 30, ACC: 0, Z: 1, N: 0} WR 13
DEBUG - {TICK: 615, ADDR: 13, IP: 30, ACC: 0, Z: 1, N: 0} WR 13
DEBUG - {TICK: 616, ADDR: 13, IP: 30, ACC: 0, Z: 1, N: 0} WR 13
DEBUG - {TICK: 617, ADDR: 13, IP: 31, ACC: 0, Z: 1, N: 0} WR 13
DEBUG - 
DEBUG - {TICK: 618, ADDR: 13, IP: 31, ACC: 0, Z: 1, N: 0} JMP 18
DEBUG - {TICK: 619, ADDR: 13, IP: 31, ACC: 0, Z: 1, N: 0} JMP 18
DEBUG - {TICK: 620, ADDR: 13, IP: 18, ACC: 0, Z: 1, N: 0} JMP 18
DEBUG - 
DEBUG - {TICK: 621, ADDR: 13, IP: 18, ACC: 0, Z: 1, N: 0} LD_REL 13
DEBUG - {TICK: 622, ADDR: 13, IP: 18, ACC: 0, Z: 1, N: 0} LD_REL 13
DEBUG - {TICK: 623, ADDR: 13, IP: 18, ACC: 0, Z: 1, N: 0} LD_REL 13
DEBUG - {TICK: 624, ADDR: 13, IP: 19, ACC: 0, Z: 1, N: 0} LD_REL 13
DEBUG - 
DEBUG - {TICK: 625, ADDR: 13, IP: 19, ACC: 0, Z: 1, N: 0} CMP 15
DEBUG - {TICK: 626, ADDR: 15, IP: 19, ACC: 0, Z: 1, N: 0} CMP 15
DEBUG - {TICK: 627, ADDR: 15, IP: 19, ACC: 0, Z: 1, N: 0} CMP 15
DEBUG - {TICK: 628, ADDR: 15, IP: 20, ACC: 0, Z: 1, N: 0} CMP 15
DEBUG - 
DEBUG - {TICK: 629, ADDR: 15, IP: 20, ACC: 0, Z: 1, N: 0} JE 32
DEBUG - {TICK: 630, ADDR: 15, IP: 20, ACC: 0, Z: 1, N: 0} JE 32
DEBUG - {TICK: 631, ADDR: 15, IP: 32, ACC: 0, Z: 1, N: 0} JE 32
DEBUG - 
DEBUG - {TICK: 632, ADDR: 15, IP: 32, ACC: 0, Z: 1, N: 0} HLT no arg
DEBUG - {TICK: 633, ADDR: 15, IP: 32, ACC: 0, Z: 1, N: 0} HLT no arg

DEBUG:root:Iteration stopped by HLT
Hello world!
instr_counter: 174 ticks: 634

```


| ФИО | алг. | LoC (в строках) | code инстр | инстр. | такт. | вариант |
| :---| :---| :---| :---| :---| :---| :---| 
| Смирнова А.Ю| hello| 26| 33| 174| 634| asm | 
| Смирнова А.Ю.| cat| 11| 7| 89| 302| asm | 
| Смирнова А.Ю.| prob5| 31| 25| 802| 3008| asm |
| Смирнова А.Ю.| hello_user | 96| 100| 406| 1487| asm | 


